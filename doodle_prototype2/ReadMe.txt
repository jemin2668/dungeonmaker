<Prototype Name>

Dungeon Maker

<Prototype High Concept>

<Team Name>
Team member	
- Lee Jaemin (jaemin.lee)
- Kim Byeongjun ()
- Lee Sangjin (sangjin.lee)

Course: GAM100F19KR
Instructor: David Ly

All content © 2019 DigiPen (USA) Corporation, all rights reserved.

Build Instruction

	When Game start, console page is opened.

	After clear required, doodle page opened.

	In doodle page player gather item, combine blocks and expand the map.


"How To Play"

	In console page, player gather the items.

	In doodle page, player combine the blocks, expand the map and escape.

Game Controls
	
	console : Using W,A,S,D to move the player four direction
	doodle :	Using  up, down, left, right arrows move the player that direction.

Credits

 - President:		Claude Comair
 - Instructor:		David Ly
 - Producer:		Lee Sangjin
 - Game Desisgner:	Lee Jaemin, Lee Sangjin
 - Main Programmer:	Lee Jaemin, Kim Byeongjun
 - Sub Programmer: 	Lee Sangjin

For Consideration

	<Type>

External Sources

	<Type>